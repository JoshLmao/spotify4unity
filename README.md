# Spotify4Unity

---

[![twitter](https://img.shields.io/badge/twitter-__JShepherd-blue.svg?style=flat-square)](https://twitter.com/_JShepherd)
[![donations](https://img.shields.io/badge/donations-_£-brightgreen.svg?style=flat-square)](https://paypal.me/ijoshlmao)
[![releases](https://img.shields.io/badge/release-v1.0-green.svg?style=flat-square)](https://assetstore.unity.com/packages/tools/integration/spotify4unity-ui-tools-spotify-authorization-129028)
[![documentation](https://img.shields.io/badge/documentation-online-brightgreen.svg?style=flat-square)](https://bitbucket.org/JoshLmao/spotify4unity/wiki/Home)

---

[Quick Start](#quick-start)
•
[Examples](#examples)
•
[Donations](#donations)

---

Spotify4Unity is a plugin which helps you integrate Spotify connectivity into you Unity game/application easily, allowing users to use basic track control functionality to viewing their whole library. This plugin relies heavily on JohnnyCrazy's [SpotifyAPI-NET](https://github.com/JohnnyCrazy/SpotifyAPI-NET)

Check out more and the full and maintained documentation [on the Wiki](https://github.com/JoshLmao/Spotify4Unity/wiki) _(Updated regularly)_

![Spotify4Unity Demo Video](repository_assets/demo.gif)

## Requirements

1. Unity 2018.2.0f2 or above
2. Scripting Runtime Version: .NET 4.6
3. Use with Windows, Mac & Linux (Android/iOS **NOT** _currently_ supported)

## Important Note

**For you to set playback, the logged in user needs to be premium**. If the user isn't, the API only allows you to get what is publicly availble like songs, playlists, user information, etc. 

## Quick Start

Check [the Home wiki page](https://github.com/JoshLmao/Spotify4Unity/wiki#Quick_Setup:) on how to quickly get started with Spotify4Unity!

## Examples

If anyone has an usage examples in any projects, [send me a message on Twitter](https://twitter.com/_JShepherd) so I can fill this section with your work! _(With your permission, of course)_

## License

Spotify4Unity is available under the [Fair Source License](https://fair.io/). Check the [License](LICENSE) for more information

## Donations

If you want to support my plugin and my works, please consider buying me a coffee (or two! ;D)

[![Donate Link](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://paypal.me/ijoshlmao)
